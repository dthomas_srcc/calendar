from django.shortcuts import render
from django.template.loader import render_to_string
from django.template import Context
from django.template.loader import get_template
from django.conf import settings as djangoSettings
import requests
import os,sys,csv,logging,json
# Create your views here.


def index(request):
    print ("[Custom Logs]: Hit on Index ")
    return render(request, "calendarcopy.html", {'output': fetchWeatherData("160549","20000101","20171212","pcpn","json")})

def Minindex(request):
    print ("[Custom Logs]: Hit on Index ")
    return render(request, "calendarcopy.html", {'output': fetchWeatherData("160718","20000101","20171212","pcpn","json")})


def Rainindex(request):
    print ("[Custom Logs]: Hit on Index ")
    return render(request, "calendarcopy.html", {'output': fetchWeatherData("166244","20000101","20171212","pcpn","json")})


def fetchWeatherData(sid, sdate, edate, elem,output):
    input_dict =  {'sid': sid, 'sdate': sdate, 'edate': edate, 'elems':elem,'output':output}
    req = requests.post("http://data.rcc-acis.org/StnData", data=json.dumps(input_dict), headers={
                    'content-type': 'application/json'}, timeout=20)
    data = req.json()
    yearData = data['data']
    print("inside function for webservice")
    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    print(yearData[0])

    with open(BASE_DIR+'/'+'core/static/django'+'.csv', "w") as output:
        writer = csv.writer(output)
        writer.writerow(["Date","rain"])
        writer.writerows(yearData)
    return yearData
